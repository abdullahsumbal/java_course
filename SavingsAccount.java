public class SavingsAccount extends AbstractAccount{
    private double balance;
    private String name;

    public SavingsAccount(){
        this("abdullah", 50);

    }

    public SavingsAccount (String name, double balance){
        super(name, balance);
        this.name = name;
        this.balance = balance;
    }

    @Override
    public void addInterest(){
        setBalance(getBalance() *  1.4);
    }

}