public abstract class  AbstractAccount implements Detailable{
    private double balance;
    private String name;
    static double interestRate = 0.1;

    public AbstractAccount(){
        this("abdullah", 50);

    }

    public AbstractAccount (String name, double balance){
        this.name = name;
        this.balance = balance;
    }

    public double getBalance() {
        return balance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public abstract void addInterest();

    public static double getInterestRate() {
        return interestRate;
    }

    public static void setInterestRate(double interestRate) {
        Account.interestRate = interestRate;
    }

    public boolean withdraw(double amount){
        if (balance >= amount){
            balance -= amount;
            return true;
        }
        return false;
    }

    public boolean withdraw(){
        return withdraw(20);
    }

    public String getDetails(){
        return "balance: " + balance;
    } 
}