import java.util.Collection;
import java.util.Collections;
import java.util.TreeSet;
public class HelloWorld{

    public static void main(String[] args) {

        Chapter3 temp = new Chapter3();
        System.out.println("--------------------------------");
        new Chapter4();
        System.out.println("--------------------------------");
        new Chapter6();
        System.out.println("--------------------------------");
        new Chapter7();
        System.out.println("--------------------------------");
        new Chapter8();
        System.out.println("--------------------------------");
        new Chapter10();
        System.out.println("--------------------------------");
        AbstractAccount [] accounts = {new CurrentAccount("a", 2), new SavingsAccount("d", 2)};
        for(AbstractAccount acc: accounts){
            acc.addInterest();
            System.out.println(acc.getBalance());
        } 
        System.out.println("--------------------------------");
        Detailable [] detailable = {new CurrentAccount("a", 2), new SavingsAccount("d", 2), new HomeInsurance(1,2,3)};
        for(Detailable d: detailable){
            
            System.out.println(d.getDetails());
        } 
        System.out.println("--------------------------------");
        CollectionTest collectionTest = new CollectionTest();
        for (AbstractAccount acc: collectionTest.accounts){
            System.out.println(acc.getBalance());
            acc.addInterest();
            System.out.println(acc.getBalance()); 
        }
        collectionTest = new CollectionTest();
        collectionTest.accounts.forEach((acc) -> {           
            System.out.println(acc.getBalance());
            acc.addInterest();
            System.out.println(acc.getBalance()); });
    
        System.out.println("--------------------------------");
        CollectionTree collectionTree = new CollectionTree();
        collectionTree.accounts.forEach((acc) -> {           
            System.out.println(acc.getBalance());
         });
        

        }
}