import java.util.Comparator;

public class AccountComparator implements Comparator<AbstractAccount>{
    
    
    @Override
    public int compare(AbstractAccount a1, AbstractAccount a2) {
        if(a1.getBalance() > a2.getBalance()){
            return 1;
        }else if(a1.getBalance() < a2.getBalance()) {
            return -1;
        }else{
            return 0;
        }
    }
    
}