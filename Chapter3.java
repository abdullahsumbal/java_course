public class Chapter3 {
    public Chapter3(){
        String make = "BMW";
        String model = "530D";
        double engineSize = 3.0;
        byte gear = 2;

        System.out.println("The make is " + make);
        System.out.println("The model is " + model);
        System.out.println("The engine size is " + engineSize);

        short speed = (short)(2 * 20);

        System.out.println(speed);
    }
}