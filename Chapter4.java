public class Chapter4 {
    public Chapter4(){
        String make = "Renault";
        String model = "Laguna";
        double engineSize = 1.8;
        byte gear = 2;
        System.out.println("The make is " + make);
        System.out.println("The model is " + model);
        System.out.println("The engine size is " + engineSize);

        if (engineSize < 1.3){
            System.out.println("weak");
        }else{
            System.out.println("strong");
        }

        for(int year =1990; year <= 2000; year+=5 ){
            System.out.println(year);
        }

        int countLeapYear = 0;
        int currentYear = 2000;
        while(countLeapYear < 5){
            currentYear += 5;
            System.out.println(currentYear);
            countLeapYear+=1;
        }
        System.out.println("finsihed");

        switch(gear){
            case 1:
            case 2:
            case 3:
            System.out.println("max speed 10");
            break;
            case 4:
            System.out.println("max speed 20"); 
            break;
        }
    }

}