public class Chapter7 {

    public Chapter7(){
        Account [] arrayOfAccounts = new Account [5];
        double[] amounts = {23,5444,2,345,34};
        String[] names = {"Picard", "Ryker", "Worf", "Troy", "Data"};

        for (int i = 0; i < names.length; i++){
            Account acc = new Account();
            acc.setBalance(amounts[i]);
            acc.setName(names[i]);
            acc.addInterest();
            arrayOfAccounts[i] = acc;
            System.out.println(arrayOfAccounts[i].getBalance());
        }
    }
    
}