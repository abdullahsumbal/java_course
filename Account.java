public class Account {
    private double balance;
    private String name;
    static double interestRate = 0.1;

    public Account(){
        this("abdullah", 50);

    }

    public Account (String name, double balance){
        this.name = name;
        this.balance = balance;
    }

    public double getBalance() {
        return balance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public void addInterest(){
        this.balance = this.balance * (1 - interestRate);
    }

    public static double getInterestRate() {
        return interestRate;
    }

    public static void setInterestRate(double interestRate) {
        Account.interestRate = interestRate;
    }

    public boolean withdraw(double amount){
        if (balance >= amount){
            balance -= amount;
            return true;
        }
        return false;
    }

    public boolean withdraw(){
        return withdraw(20);
    }
}