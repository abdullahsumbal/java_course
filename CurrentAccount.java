public class CurrentAccount extends AbstractAccount{
    private double balance;
    private String name;

    public CurrentAccount(){
        this("abdullah", 50);

    }

    public CurrentAccount (String name, double balance){
        super(name, balance);
        this.name = name;
        this.balance = balance;
    }

    @Override
    public void addInterest(){
        setBalance(getBalance() *  1.1);
    }
}