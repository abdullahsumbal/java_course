public class HomeInsurance implements Detailable{
    private double premium;
    private double  excess;
    private double amount;


    public HomeInsurance(double premium, double excess, double amount) {
        this.premium = premium;
        this.excess = excess;
        this.amount = amount;
    }

    public String getDetails(){
        return  "" + premium + " " + excess; 
    }
}