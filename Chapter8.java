public class Chapter8 {
    public Chapter8(){
        Account [] arrayOfAccounts = new Account [5];
        double[] amounts = {23,5444,2,345,34};
        String[] names = {"Picard", "Ryker", "Worf", "Troy", "Data"};
        Account.setInterestRate(0.2);
        for (int i = 0; i < names.length; i++){
            Account acc = new Account(names[i], amounts[i]);
            acc.addInterest();
            arrayOfAccounts[i] = acc;
            System.out.println(arrayOfAccounts[i].getBalance());
        } 

        System.out.println(arrayOfAccounts[4].withdraw());
        System.out.println(arrayOfAccounts[4].withdraw());
        System.out.println(arrayOfAccounts[4].withdraw());


    }
    
}